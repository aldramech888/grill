<?php 
session_start();
if(!isset($_SESSION["CONNECTED"])){
	header("location:login.php");
}
include_once("fonction.php");
$plats=getplat();
$categories=getCategorie();
$categories2=getCategorie();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>HomePage - Grill Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
			
			<?php include_once("header.php");?>
			<div class="page-content">
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
					<div class="content-box-large">
						<div class="panel-heading">
					</div>
						<div class="panel-body">
							<h1>Food</h1>
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Category</th>
									<th>Picture</th>
									<th>Action 1</th>
									<th>Action 2</th>
									<th>Action 3</th>
								</tr>
							</thead>
							<tbody>
								<?php while($plat=$plats->fetch()){ ?>
								<tr class="odd gradeX">
									<td><?php echo($plat->id);?></td>
									<td><?php echo($plat->nom);?></td>
									<td><?php echo($plat->categorie);?></td>
									<td class="center"><?php echo($plat->img);?></td>
									<td class="center"><a href="modifplat.php?id=<?php echo($plat->id);?>"><i class="glyphicon glyphicon-pencil">Modify</a></td>
									<td class="center"><a href="supprplat.php?id=<?php echo($plat->id);?>"><i class="glyphicon glyphicon-cross">Delete</a></td>
									<td class="center"><a href="recipe.php?id=<?php echo($plat->id);?>"><i class="glyphicon glyphicon-forms">See Recipe</a></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						</div>
						<div class="panel-body">
							<h1>Category</h1>
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Action 1</th>
									<th>Action 2</th>
								</tr>
							</thead>
							<tbody>
								<?php while($categorie=$categories->fetch()){ ?>
								<tr class="odd gradeX">
									<td><?php echo($categorie->id);?></td>
									<td><?php echo($categorie->nom);?></td>
									<td class="center"><a href="modifcat.php?id=<?php echo($categorie->id);?>"><i class="glyphicon glyphicon-pencil">Modify</a></td>
									<td class="center"><a href="supprcat.php?id=<?php echo($categorie->id);?>"><i class="glyphicon glyphicon-cross">Delete</a></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						</div>
					</div>
						<div class="col-md-5 col-md-offset-1">
							<div class="content-box-large">
										<div class="panel-heading">
													<div class="panel-title"><h2>New Category</h2></div>
												
													<div class="panel-options">
														<a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
														<a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
													</div>
											</div>
										<div class="panel-body">
											<form class="form-horizontal" action="newcat.php" method="post" role="form">
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-2 control-label">Name</label>
												<div class="col-sm-10">
													<input type="text" name="nom" class="form-control" id="inputEmail3" placeholder="Name">
												</div>
											</div>																																					
											<div class="form-group">
												<div class="col-sm-offset-2 col-sm-10">
													<button type="submit" class="btn btn-primary">Insert</button>
												</div>
											</div>
										</form>
										</div>
									</div>
							</div>
						<div class="col-md-10 col-md-offset-1">
						<div class="content-box-large">
										<div class="panel-heading">
													<div class="panel-title"><h2>New Food</h2></div>
													<div class="panel-body">
													<h4>Ingredients</h4>
															<textarea name="ingr" form="usrform" id="ckeditor_full"></textarea>
													</div>	
													<div class="panel-title"><h4>Preparation</h4></div>
													<div class="panel-body">
															<textarea name="prep" form="usrform" id="ckeditor_full"></textarea>
													</div>
													<div class="panel-options">
														<a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
														<a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
													</div>
											</div>
										<div class="panel-body">
											<form class="form-horizontal" id="usrform" action="newplat.php" method="post" role="form">
											<div class="form-group">
												<label for="inputEmail3"  class="col-sm-2 control-label">Name</label>
												<div class="col-sm-10">
													<input type="text" name="nom" class="form-control" id="inputEmail3" placeholder="Name">
												</div>
											</div>
											<div class="form-group">
												<label for="inputEmail3" class="col-sm-2 control-label">Category</label>
												<div class="col-sm-10">
														<select class="form-control" name="cat">
																<?php while($categorie=$categories2->fetch()){ ?>
																		<option value="<?php echo($categorie->id);?>"><?php echo($categorie->nom);?></option>
																<?php } ?>
														</select>
												</div>
												</div>
												<div class="form-group">
												<label for="inputEmail3" class="col-sm-2 control-label">Picture</label>
												<div class="col-sm-10">
														<input type="text" name="img" class="form-control" id="inputEmail3" placeholder="Link of the Picture">
												</div>
												</div>
																																																
											<div class="form-group">
												<div class="col-sm-offset-2 col-sm-10">
													<button type="submit" class="btn btn-primary">Insert</button>
												</div>
											</div>
										</form>
										</div>
									</div>
							</div>
						</div>
				
			</div>
		</div>
	</div>
			<?php include_once("footer.php");?>

<link rel="stylesheet" type="text/css" href="vendors/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css"></link> 

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery.js"></script>
<!-- jQuery UI -->
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<script src="vendors/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
<script src="vendors/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js"></script>

<script src="vendors/ckeditor/ckeditor.js"></script>
<script src="vendors/ckeditor/adapters/jquery.js"></script>

<script type="text/javascript" src="vendors/tinymce/js/tinymce/tinymce.min.js"></script>

<script src="js/custom.js"></script>
<script src="js/editors.js"></script>
  </body>
</html>