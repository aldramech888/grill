<?php 
include_once("fonction.php");
$plats=getplatCond(" where id=".$_GET["id"]);
$plat=$plats->fetch();
$categories=getCategorie();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Food Update - Grill Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
			
			<?php include_once("header.php");?>
    <div class="page-content">
    	<div class="row">
            <div class="col-md-6">
                <div class="content-box-large">
                    <div class="panel-heading">
                        <div class="panel-title">Modify the food</div>
                        
                        <div class="panel-options">
                            <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                            <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="traitplat.php?id=<?php echo($_GET["id"]);?>" method="post" role="form">
                            <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="nom" class="form-control" id="inputEmail3" value="<?php echo($plat->nom);?>">
                            </div>
                            </div>
                            <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Category</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="cat">
                                    <?php while($categorie=$categories->fetch()){ ?>
                                        <option value="<?php echo($categorie->id);?>"><?php echo($categorie->nom);?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            </div>
                            <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">Picture</label>
                            <div class="col-sm-10">
                                <input type="text" name="img" class="form-control" id="inputEmail3" value="<?php echo($plat->img);?>">
                            </div>
                            </div>                                                                            
                            <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <a href="index.php" class="btn btn-default btn-md">Cancel</a>
                                <button type="submit" class="btn btn-primary">Modify</button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>				
		</div>
    </div>
			<?php include_once("footer.php");?>

<link href="vendors/datatables/dataTables.bootstrap.css" rel="stylesheet" media="screen">

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery.js"></script>
<!-- jQuery UI -->
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<script src="vendors/datatables/js/jquery.dataTables.min.js"></script>

<script src="vendors/datatables/dataTables.bootstrap.js"></script>

<script src="js/custom.js"></script>
<script src="js/tables.js"></script>
  </body>
</html>