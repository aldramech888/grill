<?php
include_once("fonction.php");
$id=$_GET["id"];
$plats=getplatCond(" where id=".$id);
$plat=$plats->fetch();

$recettes=getrecetteCond(" where plat=".$id);
$recette=$recettes->fetch();

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Recipe Editor - Grill Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- jQuery UI -->
    <link href="https://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet" media="screen">

    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- styles -->
    <link href="css/styles.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <?php include_once("header.php");?>
    <div class="page-content">
    	<div class="row">
            <h1><?php echo($plat->nom);?></h1>
				<div class="col-md-10">
                <div class="content-box-large">
                <div class="panel-heading">
                <div class="panel-title"><h2>Ingredients</h2></div>
                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                    <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                </div>
                </div>
                <div class="panel-body">
                    <textarea name="ingr" form="usrform" id="ckeditor_full"><?php echo($recette->ingredients);?></textarea>
                </div>
                </div>
                </div>

                <div class="col-md-10">
                <div class="content-box-large">
                <div class="panel-heading">
                <div class="panel-title"><h2>Recipe</h2></div>
                <div class="panel-options">
                    <a href="#" data-rel="collapse"><i class="glyphicon glyphicon-refresh"></i></a>
                    <a href="#" data-rel="reload"><i class="glyphicon glyphicon-cog"></i></a>
                </div>
                </div>
                <div class="panel-body">
                    <textarea name="prep" form="usrform" id="ckeditor_full"><?php echo($recette->preparation);?></textarea>
                </div>
                
                <form id="usrform" method="post" action="modif.php?id=<?php echo($recette->id);?>">
                    <a href="index.php" class="btn btn-default btn-md">Cancel</a>
                    <input type="submit" value="Save" class="btn btn-primary btn-md">
                </form>
                </div>
                </div>
                
        </div>
    </div>
  <?php include_once("footer.php");?>  
<link rel="stylesheet" type="text/css" href="vendors/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css"></link> 

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery.js"></script>
<!-- jQuery UI -->
<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bootstrap/js/bootstrap.min.js"></script>

<script src="vendors/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>
<script src="vendors/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js"></script>

<script src="vendors/ckeditor/ckeditor.js"></script>
<script src="vendors/ckeditor/adapters/jquery.js"></script>

<script type="text/javascript" src="vendors/tinymce/js/tinymce/tinymce.min.js"></script>

<script src="js/custom.js"></script>
<script src="js/editors.js"></script>
</body>
</html>