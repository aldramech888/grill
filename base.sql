use grill;

create table Categorie
(
	id int not null auto_increment primary key,
    nom varchar(100)
);


create table plat
(
	id int not null auto_increment primary key,
    nom varchar(50),
    categorie int,
    img varchar(50)
);

create table recette
(
	id int not null auto_increment primary key,
    plat int,
    ingredients text,
    preparation text
);

1 insert into categorie values ('','Drink');
2 insert into categorie values ('','Soup');
3 insert into categorie values ('','Dessert');
4 insert into categorie values ('','Sandwich');

1   insert into plat values ('','Cr&egrave;me br&ucirc;l&eacute;e',3,'product1.jpg');
    insert into recette values('',1,
    "<ul><li>Sugar</li>
    <li>2 vanilla beans</li>
    <li>2 cups of heavy cream</li>
    <li>6 egg yolks</li>
    <li>Raw sugar</li></ul>",
    "<ol><li>Add 2 cups of sugar to a food processor. Cut a vanilla bean lengthwise, scrape out the insides with a spoon, and add to the food processor. Pulse until fully combined.</li>
    <li>In a medium sized pot add 75g of vanilla sugar and 2 cups of heavy cream. Cut a vanilla bean lengthwise, scrape out the insides with a spoon, and add to the pot along with the bean itself. Bring to a medium heat and stir until simmering. As soon as it starts simmering, strain and set aside to cool for 15 minutes.</li>
    <li>Next, seperate 6 egg yolks and whisk together in a bowl while SLOWLY pouring in your vanilla cream mixture.</li>
    <li>Take a kitchen towel and place in a deep casserole dish. Add vanilla cream and egg mixture to 6 ramekins and place them on top of towel in the casserole dish. Fill the casserole dish ⅔ or the way up on the ramekins with boiling water.</li>
    <li>Place in a 300°F oven and bake for 24-40 minutes depending on the size and depth of your ramekins. (Remove with they reach 175°F)</li>
    <li>Remove from the oven and let them cool for 1 hour or until room temperature. Once they’ve reached room temperature, place on a baking sheet, cover, and place in the fridge for 4 hours.</li>
    <li>After they’ve been in the fridge for at least 4 hours, remove and place raw sugar on top of your creme. Using a torch, slowly torch the top of the sugar to caramelize it and make it hard.</li>
    <li>Re-refrigerate for 45 minutes.</li>
    <li>Remove from the fridge. Serve and enjoy!</li></ol>
    "
    );

2   insert into plat values ('','Clementine Cake',3,'product2.jpg');
    insert into recette values('',2,
    "<ul><li>Clementines</li>
    <li>3 cups of sugar</li>
    <li>450g of cake flour</li>
    <li>1 ½ tsp salt</li>
    <li>¾ tsp of baking powder</li>
    <li>¾ tsp baking soda</li>
    <li>1 ½ cup of butter</li>
    <li>487.5g of sugar</li>
    <li>6 large eggs</li>
    <li>3 tsp vanilla extract</li>
    <li>¾ cup clementine juice</li>
    <li>Zest of 4 clementines</li>
    <li>1 ¼ cup buttermilk</li>
    <li>Nonstick spray</li>
    <li>Parchment paper</li></ul>
    <p>Clementine Glaze</p>
    <ul><li>150g sifted powdered sugar</li><li>30-45 ml of clementine juice</li><li>Zest of 1 clementine</li></ul>",
    "<ol><li>Start by slicing 3 clementines for candying. Make sure you slice them very thin.</li><li>Poach the clementines in boiling water for 1 minute, and then shock them by transferring them to a bowl of ice water.</li><li>Combine 3 cups of sugar and 1 ½ cups of water in a large saucepan. Add a little squeeze of clementine juice and bring to a sweet gentle simmer. Add clementines in a single layer and simmer for 90 minutes, flipping every 30 minutes.</li><li>Once soft and translucent cool on a wire rack, and pour clementine syrup into a measuring cup for later, and set aside.</li><li>Weigh out 450g of cake flour to which you’re going to add 1 ½ tsp of salt, ¾ tsp of baking powder, and ¾ tsp of baking soda. Whisk to combine.</li><li>Next, add 1 ½ cups of butter to the bowl of a stand mixer, and mix until creamy.</li><li>Scrape down the sides of the bowl and add 487.5g of sugar and mix that as well.</li><li>Scrape down the sides again, and add 6 large eggs, 3 tsp of vanilla extract, and mix.</li><li>Add ¾ cup of clementine juice and the zest of 4 clementines, and mix to combine.</li><li>Finally add the dry ingredients and 1 ¼ cups of buttermilk and mix just enough to combine.</li><li>Coat a cake pan with nonstick spray and parchment paper. Add the cake batter and cook at 350°F for 45 minutes.</li><li>Use a cake tester or a knife to make sure the cake is completely done.</li><li>Generously brush down the cake with clementine syrup.</li><li>Pour glaze over the top and add candied clementines.</li><li>Slice yourself a piece and enjoy.</li></ol><p>Clementine Glaze</p><ol><li>Combine 150g of sifted powdered sugar, 30-45 ml of clementine juice, and the zest of one clementine. Whisk to combine. (You can use milk instead of juice to look more accurate to the movie, but the juice is better)</li></ol>");


