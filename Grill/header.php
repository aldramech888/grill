<header>
    <div id="main-header">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="logo">
                        <a href="#"><img src="images/logo.png" title="Grill Template" alt="Grill Website Template" ></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="main-menu">
                        <ul>
                            <li><a href="grill_homepage">Home</a></li>
                            <li><a href="grill_recipe_list">Recipes</a></li>
                            <li><a href="about-the-author-of-the-website">About</a></li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
    </div>
</header>