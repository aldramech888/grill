<?php
include_once("fonction.php");
$plats=getplatCond(" order by id desc limit 6");
?>
<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <title>Homepage - Grill</title>
        <meta name="description" content="Grill has the best collection of recipes online. Whether you have ten minutes or two hours, we have mouth-watering recipes that will inspire and delight.">
        <meta name="keywords" content="recipes, food, drinks, desserts, sites"> 
        <meta name="viewport" content="width=device-width">
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/templatemo_style.css">
        <link rel="stylesheet" href="css/templatemo_misc.css">
        <link rel="stylesheet" href="css/flexslider.css">
        <link rel="stylesheet" href="css/testimonails-slider.css">

        <script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
    </head>
    <body>
        
        <?php include_once("header.php");?>

            <div id="slider">
                <h1>HOMEPAGE</h1>
                <div class="flexslider">
                  <ul class="slides">
                    <li>
                        <div class="slider-caption">
                            <p>Delicious Meals</p>
                            <p>Discover recipes of dishes from all over the world with precise 
                            <br><br>preparation methods and ingredients.</p>
                        </div>
                      <img src="images/slide1.jpg" alt="Delicious Meals" />
                    </li>
                    <li>
                        <div class="slider-caption">
                            <p>Original Menus</p>
                            <p>Impress and surprise your friends and familiy
                            <br><br>with brand new original meals.</p>
                        </div>
                      <img src="images/slide2.jpg" alt="Original Menus" />
                    </li>
                    <li>
                        <div class="slider-caption">
                            <p>Healthy Drinks</p>
                            <p>Drench your thirst with all sorts of cocktail, juice and wine 
                            <br><br>best consumed with those you love.</p>
                        </div>
                      <img src="images/slide3.jpg" alt="Healthy Drinks" />
                    </li>
                  </ul>
                </div>
            </div>

            <div id="latest-blog">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-section">
                                <h2>Latest Recipe posts</h2>
                                <img src="images/under-heading.png" alt="" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php while($plat=$plats->fetch()){?>
                            <div class="col-md-4 col-sm-6">
                                <div class="blog-post">
                                    <div class="blog-thumb">
                                        <img src="images/<?php echo ($plat->img);?>" alt="" />
                                    </div>
                                    <div class="blog-content">
                                        <div class="content-show">
                                            <h4><a href="how-to-prepare-<?php echo($plat->nom);?>-<?php echo($plat->id);?>"><?php echo ($plat->nom);?></a></h4>
                                        </div>
                                        <div class="content-hide">
                                            <p>New Recipe!</p>
                                         </div>                                        
                                    </div>
                                </div>
                            </div>  
                        <?php } ?>
                    </div>
                </div>
            </div>

        <?php include_once("footer.php");?>
        <script src="js/vendor/jquery-1.11.0.min.js"></script>
        <script src="js/vendor/jquery.gmap3.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>