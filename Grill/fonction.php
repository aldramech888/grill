<?php
function getConnectionBase(){
    $user='root';
    $mdp='root';
    $dsn='mysql:host=localhost;port=3306;dbname=grill';
    $connexion=new PDO($dsn, $user, $mdp);
    return $connexion;
}
// Categorie
function getCategorie(){
    $connexion=getConnectionBase();
    $sql1="SELECT * from categorie";
    $sql1=sprintf($sql1);
    $res =$connexion->query($sql1);
    $res->setFetchMode(PDO::FETCH_OBJ);
    return $res;
}
function getCategorieCond($cond){
    $connexion=getConnectionBase();
    $sql1="SELECT * from categorie %s";
    $sql1=sprintf($sql1,$cond);
    $res =$connexion->query($sql1);
    $res->setFetchMode(PDO::FETCH_OBJ);
    return $res;
}
function insertCategorie($nom){
                    $connexion=getConnectionBase();
                    $sql2="INSERT into categorie values('','%s')";
                    $sql2=sprintf($sql2,$nom);
                    $res2=$connexion->exec($sql2);
    $res2->closeCursor();			
}
function updateCategorie($id,$nom){
    $connexion=getConnectionBase();
    $sql2="update categorie set nom='%s' where id=%s ";
    $sql2=sprintf($sql2,$nom,$id);
    $res2=$connexion->exec($sql2);
    $res2->closeCursor();			
}
function deleteCategorie($id){
    $connexion=getConnectionBase();
    $sql2="delete from categorie where id=%s ";
    $sql2=sprintf($sql2,$id);
    $res2=$connexion->exec($sql2);
    $res2->closeCursor();			
}
// /Categorie

// Plat
function getplat(){
    $connexion=getConnectionBase();
    $sql1="SELECT * from plat";
    $sql1=sprintf($sql1);
    $res =$connexion->query($sql1);
    $res->setFetchMode(PDO::FETCH_OBJ);
    return $res;
}
function getplatCond($cond){
    $connexion=getConnectionBase();
    $sql1="SELECT * from plat %s";
    $sql1=sprintf($sql1,$cond);
    $res =$connexion->query($sql1);
    $res->setFetchMode(PDO::FETCH_OBJ);
    return $res;
}
function insertplat($nom,$descr,$prix,$categorie,$img){
                    $connexion=getConnectionBase();
                    $sql2="INSERT into plat values('','%s','%s',%s,%s,'%s')";
                    $sql2=sprintf($sql2,$nom,$descr,$prix,$categorie,$img);
                    $res2=$connexion->exec($sql2);
    $res2->closeCursor();			
}
function updateplat($id,$nom,$descr,$prix,$categorie,$img){
    $connexion=getConnectionBase();
    $sql2="update plat set nom='%s',descr='%s',prix=%s,categorie=%s,img='%s' where id=%s ";
    $sql2=sprintf($sql2,$nom,$descr,$prix,$categorie,$img,$id);
    $res2=$connexion->exec($sql2);
    $res2->closeCursor();			
}
function deletePlat($id){
    $connexion=getConnectionBase();
    $sql2="delete from plat where id=%s ";
    $sql2=sprintf($sql2,$id);
    $res2=$connexion->exec($sql2);
    $res2->closeCursor();			
}
// /Plat

// Recette
function getrecette(){
    $connexion=getConnectionBase();
    $sql1="SELECT * from recette";
    $sql1=sprintf($sql1);
    $res =$connexion->query($sql1);
    $res->setFetchMode(PDO::FETCH_OBJ);
    return $res;
}
function getrecetteCond($cond){
    $connexion=getConnectionBase();
    $sql1="SELECT * from recette %s";
    $sql1=sprintf($sql1,$cond);
    $res =$connexion->query($sql1);
    $res->setFetchMode(PDO::FETCH_OBJ);
    return $res;
}
function insertrecette($nom){
                    $connexion=getConnectionBase();
                    $sql2="INSERT into recette values('','%s')";
                    $sql2=sprintf($sql2,$nom);
                    $res2=$connexion->exec($sql2);
    $res2->closeCursor();			
}
function updaterecette($id,$nom){
    $connexion=getConnectionBase();
    $sql2="update recette set nom='%s' where id=%s ";
    $sql2=sprintf($sql2,$nom,$id);
    $res2=$connexion->exec($sql2);
    $res2->closeCursor();			
}
function deleterecette($id){
    $connexion=getConnectionBase();
    $sql2="delete from recette where id=%s ";
    $sql2=sprintf($sql2,$id);
    $res2=$connexion->exec($sql2);
    $res2->closeCursor();			
}
// /Recette
?>