<?php
include_once("fonction.php");
$plats=getPlat();
$categories=getCategorie();
?>
<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <title>List of all Recipes - Grill</title>
        <meta name="description" content="Grill has the best collection of recipes online. Whether you have ten minutes or two hours, we have mouth-watering recipes that will inspire and delight.">
        <meta name="keywords" content="recipes, food, drinks, desserts, sites"> 
        <meta name="viewport" content="width=device-width">
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/templatemo_style.css">
        <link rel="stylesheet" href="css/templatemo_misc.css">
        <link rel="stylesheet" href="css/flexslider.css">
        <link rel="stylesheet" href="css/testimonails-slider.css">

        <script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
    </head>
    <body>
        
        <?php include_once("header.php");?>


<div id="products-post">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="product-heading">
                    <h1>List of all Recipes</h1>
                    <img src="images/under-heading.png" alt="" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="filters col-md-12 col-xs-12">
                <ul id="filters" class="clearfix">
                    <li><span class="filter" data-filter="all">All</span></li>
                    <?php while($categorie=$categories->fetch()){ ?>
                    <li><span class="filter" data-filter=".cat<?php echo($categorie->id);?>"><?php echo($categorie->nom);?></span></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="row" id="Container">
            <?php while($plat=$plats->fetch()){?>
            <div class="col-md-3 col-sm-6 mix portfolio-item cat<?php echo($plat->categorie);?>">       
                <div class="portfolio-wrapper">                
                    <div class="portfolio-thumb">
                        <img src="images/<?php echo($plat->img);?>" alt="<?php echo($plat->img);?>" />
                        <div class="hover">
                            <div class="hover-iner">
                                <a class="fancybox" href="#"><img src="images/open-icon.png" alt="" /></a>
                                <span><?php echo($plat->nom);?></span>
                            </div>
                        </div>
                    </div>  
                    <div class="label-text">
                        <p><span style="font-size:20px"><a href="how-to-prepare-<?php echo($plat->nom);?>-<?php echo($plat->id);?>"><?php echo($plat->nom);?></a></p>
                    </div>
                </div>          
            </div>
            <?php }?>
        </div>            
    </div>
</div>



        <?php include_once("footer.php");?>
        <script src="js/vendor/jquery-1.11.0.min.js"></script>
        <script src="js/vendor/jquery.gmap3.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>