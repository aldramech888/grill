<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <title>About the author of the Website - Grill</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/templatemo_style.css">
        <link rel="stylesheet" href="css/templatemo_misc.css">
        <link rel="stylesheet" href="css/flexslider.css">
        <link rel="stylesheet" href="css/testimonails-slider.css">

        <script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
    </head>
    <body>
        
        <?php include_once("header.php");?>
        
        
        <div id="our-team">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-section">
                                <h1>About the Author of the Website</h1>
                                <img src="images/under-heading.png" alt="" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                        <img src="images/Profile.jpg" alt="" >
                        </div>
                        <div class="col-md-6 col-md-offset-1">
                        Ramilison Andrianoavy Abraham is one part chef, one part filmmaker, and a generous dash of irreverent YouTube personality.  Self-taught both behind and in front of the camera, his cooking show, MadaGrill, is enjoyed by millions of burgeoning chefs and foodies around the globe.  his passion for teaching and experimenting in the kitchen is rivaled only by his love of film and television, both of which he endeavors to share from his Andoharanofotsy, Antananarivo.
                        </div>
                    </div>
                </div>
            </div>

        <?php include_once("footer.php");?>
        <script src="js/vendor/jquery-1.11.0.min.js"></script>
        <script src="js/vendor/jquery.gmap3.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>