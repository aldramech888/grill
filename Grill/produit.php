<?php
include_once("fonction.php");

$id = $_GET['id'];
$plats = getplatCond(" where id=".$id);
$plat=$plats->fetch();
$recettes = getrecetteCond(" where plat=".$id);
$recette=$recettes->fetch();
$categories = getCategorieCond(" where id=".$plat->categorie);
$categorie=$categories->fetch();
?>
<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <title>How to prepare <?php echo($plat->nom);?>? - Grill</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <meta charset="UTF-8">
        
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/templatemo_style.css">
        <link rel="stylesheet" href="css/templatemo_misc.css">
        <link rel="stylesheet" href="css/flexslider.css">
        <link rel="stylesheet" href="css/testimonails-slider.css">

        <script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
    </head>
    <body>
        
        <?php include_once("header.php");?>
        <!--Contenu-->
        <div id="product-post">
                <div class="container">
                <div id="heading">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-content">
                                <!--<h3><span>Product / <?php //echo($categorie->nom);?> / <?php //echo($plat->nom);?></span></h3>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-section">
                                
                            <h1>How to prepare <?php echo($plat->nom);?>?</h1>
                            <h4><span>Recipes / <?php echo($categorie->nom);?> / <?php echo($plat->nom);?></span></h4>
                                <img src="images/under-heading.png" alt="under heading" />
                            </div>
                        </div>
                    </div>
                    <div id="single-blog" class="page-section first-section">
                        <div class="container">
                            <div class="row">
                                <div class="product-item col-md-12">
                                    <div class="row">
                                        <div>  
                                                <div class="image">
                                                    <div class="image-post">
                                                        <img src="images/<?php echo($plat->img);?>" alt="<?php echo($plat->img);?>">
                                                    </div>
                                                </div>
                                                <div class="product-content">
                                                    <div class="product-title">
                                                        <h2><?php echo($plat->nom);?></h2>
                                                    </div>
                                                    <div class="col-md-4"><?php?>
                                                        <h3>Ingredients</h3>
                                                        <?php echo($recette->ingredients);?>
                                                    </div>
                                                    <div class="col-md-6 col-md-offset-1">
                                                        <h3>Method</h3>
                                                        <?php echo($recette->preparation);?>
                                                    </div>
                                                </div>                                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>     
                </div>
            </div>    
        <!--//Contenu-->
        <?php include_once("footer.php");?>
        <script src="js/vendor/jquery-1.11.0.min.js"></script>
        <script src="js/vendor/jquery.gmap3.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>