<footer>
    <div class="container">       
        <div class="main-footer">
            <div class="row">
                <div class="col-md-3">
                    <div class="about">
                        <p class="footer-title">About Grill</p>
                        <p>Grill is the number one website for those who seek new tasty meals and drinks and it is a free website to follow recipes.                        
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="shop-list">
                        <p class="footer-title">Shop Categories</p>
                        <ul>
                            <li><a href="#"><i class="fa fa-angle-right"></i>New Grill Menu</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i>Healthy Fresh Juices</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i>Spicy Delicious Meals</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i>Simple Italian Pizzas</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i>Pure Good Yogurts</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i>Ice-cream for kids</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                <div class="social-bottom">
                    <p class="footer-title">Follow us:</p>
                    <ul>
                        <li><a href="#" class="fa fa-facebook"></a></li>
                        <li><a href="#" class="fa fa-twitter"></a></li>
                        <li><a href="#" class="fa fa-rss"></a></li>
                    </ul>
                </div>
                </div>
                <div class="col-md-3">
                    <div class="more-info">
                        <p class="footer-title">More info</p>
                        <p>"A full belly, catches up to his friends."</p>
                        <ul>
                            <li><i class="fa fa-phone"></i>010-020-0340</li>
                            <li><i class="fa fa-globe"></i>123 Dagon Studio, Yakin Street, Digital Estate</li>
                            <li><i class="fa fa-envelope"></i><a href="#">info@company.com</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footer">
            <p>
                <span>Copyright © 2018 - Ramilison Andrianoavy Abraham n28 ETU000522</span> 
            </p>
        </div>
        
    </div>
</footer>